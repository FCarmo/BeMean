'use strict'

const express = require('express')
const app = express();

app.get('/', function (req, res) {
    res.set('Content-Type','text/plain');
    res.send('hello word');
});

app.get('*', function (req, res) {
    res.set({
        'Content-Type': 'text/plain',
        'Content-Length': '3',
        'ETag' : '12345'
    })
    res.send('oi');
})

app.listen(3000, function () {
    console.log('Servidor rodando na porta 3000')
})