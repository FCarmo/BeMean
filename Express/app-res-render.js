const express = require('express');
const app = express();

app.set('view engine','jade')

app.get('/users', function (req, res) {
    const users = [
      { name: 'Suissa' }
    , { name: 'Itacir' }
    , { name: 'Caio' }
    ];
    res.render('users/views/index', {users} );
  });

app.listen(3000, function () {
    console.log('Servidor rodando em localhost:3000');
  });
  