const express = require('express');
const app = express();
const path = require('path');

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'modules/users/views/list.html'));
})

app.listen(3000, function () {
    console.log('Servidor rodando na porta 3000');
});